package erlhoyAcademy.SpringBootApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITunesBetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ITunesBetaApplication.class, args);
	}

}
