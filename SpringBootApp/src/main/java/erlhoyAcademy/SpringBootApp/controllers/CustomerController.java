package erlhoyAcademy.SpringBootApp.controllers;

import erlhoyAcademy.SpringBootApp.data_access.models.*;
import erlhoyAcademy.SpringBootApp.data_access.repositories.CustomerRepository;
import org.springframework.web.bind.annotation.*;

import  java.util.List;

@RestController
public class CustomerController {

    final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("api/customer/all")
    public List<Customer> getCustomers() {
        return customerRepository.getCustomersFromDatabase();
    };

    @GetMapping("api/customer/id={id}")
    public Customer getCustomers(@PathVariable int id) {
        return customerRepository.getCustomerFromDatabaseById(id);
    };

    @GetMapping("api/customer/name={name}")
    public Customer getCustomerByName(@PathVariable String name) {
        return customerRepository.getCustomerFromDatabaseByName(name);
    };

    @GetMapping("api/customer/page/limit={limit}offset={offset}")
    public List<Customer> getCustomerPage(@PathVariable int limit, @PathVariable int offset) {
        return customerRepository.getCustomerPageFromDatabase(limit, offset);
    };

    @PostMapping("api/customer")
    public boolean addNewCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomerToDatabase(customer);
    };

    @PutMapping("api/customer")
    public boolean updateCustomer(@RequestBody Customer customer) {
        return customerRepository.updateCustomerFromDatabase(customer);
    };

    @GetMapping("api/customer/count")
    public List<CustomerCountry> getCount() {
        return customerRepository.getNumberOfCustomersInEachCountry();
    }

    @GetMapping("api/customer/spender")
    public List<CustomerSpender> getHighestSpenders() {
        return customerRepository.getHighestSpendersCustomers();
    };

    @GetMapping("api/customer/popular/genre")
    public CustomerGenre getCustomerPopularGenre(@RequestBody Customer customer) {
        return customerRepository.getCustomerPopularGenre(customer);
    };
}
