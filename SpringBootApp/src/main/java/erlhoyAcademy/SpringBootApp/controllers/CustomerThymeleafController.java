package erlhoyAcademy.SpringBootApp.controllers;

import erlhoyAcademy.SpringBootApp.data_access.models.*;
import erlhoyAcademy.SpringBootApp.data_access.repositories.SongInformationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class CustomerThymeleafController {

    final SongInformationRepository songInformationRepository;

    CustomerThymeleafController(SongInformationRepository songInformationRepository) {
        this.songInformationRepository = songInformationRepository;
    }

    @GetMapping("/")
    public String viewHome(Model model) {

        // The functions in songInformationRepository, connects to db, executes prepared statement, and returns a list of 5 respective class types
        List<Artist> artistList = songInformationRepository.getRandomArtists(); // get 5 random artists from database
        List<Song> songList = songInformationRepository.getRandomSongs(); // get 5 random songs from database
        List<Genre> genreList = songInformationRepository.getRandomGenres(); // get 5 random genres from database

        model.addAttribute("artists", artistList);
        model.addAttribute("songList", songList);
        model.addAttribute("genreList", genreList);
        model.addAttribute("inputSearch", new InputSearch());

        return "home";
    }

    @PostMapping("/song/search")
    public String viewSearchResults(@ModelAttribute InputSearch inputSearch, BindingResult errors, Model model) {

        SongArtistAlbumGenre songTableInfo = songInformationRepository.getSongTableInfo(inputSearch.getInputText());
        model.addAttribute("songTableInfo", songTableInfo);

        return "search_results";
    }

}
