package erlhoyAcademy.SpringBootApp.data_access;

import org.springframework.stereotype.Service;

@Service
public class ConnectionURL {
    public static final String URL_STRING = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
}
