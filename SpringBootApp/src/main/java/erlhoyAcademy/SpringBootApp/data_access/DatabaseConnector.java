package erlhoyAcademy.SpringBootApp.data_access;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnector {

    protected String URL = ConnectionURL.URL_STRING;
    protected Connection con = null;

    public void connectToDatabase() {
        try {
            con = DriverManager.getConnection(URL);
        } catch (SQLException e) {}
    }

    public void closeDatabaseConnection() {
        try {
            con.close();
        } catch (SQLException e) {}
    }
}
