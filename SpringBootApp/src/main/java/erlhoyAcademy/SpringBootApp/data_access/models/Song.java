package erlhoyAcademy.SpringBootApp.data_access.models;

public class Song {

    private String songId;
    private String songName;

    public Song(String songId, String songName) {
        this.songId = songId;
        this.songName = songName;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }
}
