package erlhoyAcademy.SpringBootApp.data_access.models;

public class SongArtistAlbumGenre {

    private String songName;
    private String artistName;
    private String albumName;
    private String genreName;
    private boolean success = false;

    public SongArtistAlbumGenre(String songName, String artistName, String albumName, String genreName, boolean success) {
        this.songName = songName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.genreName = genreName;
        this.success = success;
    }

    public SongArtistAlbumGenre(String songName) {
        this.songName = songName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
