package erlhoyAcademy.SpringBootApp.data_access.models;

public class CustomerCountry {

    private String country;
    private int customerCount;

    public CustomerCountry(String country, int customerCount) {
        this.country = country;
        this.customerCount = customerCount;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(int customerCount) {
        this.customerCount = customerCount;
    }
}
