package erlhoyAcademy.SpringBootApp.data_access.models;

public class InputSearch {

    private String inputText;

    public String getInputText() {
        return inputText;
    }
    public void setInputText(String input) { this.inputText = input; }
}
