package erlhoyAcademy.SpringBootApp.data_access.models;

public class CustomerSpender {

    private String customerId;
    private String firstName;
    private String lastName;
    private String invoiceTotal;

    public CustomerSpender(String customerId, String firstName, String lastName, String invoiceTotal) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.invoiceTotal = invoiceTotal;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }
}
