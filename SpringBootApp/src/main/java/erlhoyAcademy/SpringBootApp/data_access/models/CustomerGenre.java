package erlhoyAcademy.SpringBootApp.data_access.models;

public class CustomerGenre {

    private String customerId;
    private String firstName;
    private String lastName;
    private String popularGenre;

    public CustomerGenre(String customerId, String firstName, String lastName, String popularGenre) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.popularGenre = popularGenre;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPopularGenre() {
        return popularGenre;
    }

    public void setPopularGenre(String popularGenre) {
        this.popularGenre = popularGenre;
    }
}
