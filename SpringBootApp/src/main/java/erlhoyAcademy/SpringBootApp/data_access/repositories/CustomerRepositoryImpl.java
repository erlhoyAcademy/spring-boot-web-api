package erlhoyAcademy.SpringBootApp.data_access.repositories;

import erlhoyAcademy.SpringBootApp.data_access.DatabaseConnector;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerCountry;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerGenre;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerSpender;
import erlhoyAcademy.SpringBootApp.logging.ConsoleLogger;
import erlhoyAcademy.SpringBootApp.data_access.models.Customer;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl extends DatabaseConnector implements CustomerRepository{

    @Override
    public List<Customer> getCustomersFromDatabase() {
        List<Customer> customerList = new ArrayList<>();
        String prepStmtString = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            ResultSet resultSet = con.prepareStatement(prepStmtString).executeQuery();
            while(resultSet.next()) {
                customerList.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return customerList;
    }

    @Override
    public Customer getCustomerFromDatabaseById(int id) {
        Customer customer = null;
        String prepStmtString = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE CustomerId=?";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return customer;
    }

    @Override
    public Customer getCustomerFromDatabaseByName(String name) {
        Customer customer = null;
        String prepStmtString = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE FirstName LIKE ?";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return customer;
    }

    @Override
    public List<Customer> getCustomerPageFromDatabase(int limit, int offset) {
        List<Customer> customerList = new ArrayList<>();
        String prepStmtString = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer LIMIT ? OFFSET ?";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customerList.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email"))
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return customerList;
    }

    @Override
    public boolean addCustomerToDatabase(Customer customer) {

        boolean success = false;
        String prepStmtString = "INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?, ?, ?, ?, ?, ?, ?)"; // 7 fields

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                ConsoleLogger.log("Sucessfully added a new customer to database");
                success = true;
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return success; // boolean value (true of false, depending on query operation success)
    };

    @Override
    public boolean updateCustomerFromDatabase(Customer customer) {

        boolean success = false;
        String prepStmtString = "UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setString(8, customer.getCustomerId()); // fill in: WHERE id = ?

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                ConsoleLogger.log("Sucessfully updated the customer in the database");
                success = true;
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return success; // boolean value (true of false, depending on query operation success)
    }

    @Override
    public List<CustomerCountry> getNumberOfCustomersInEachCountry() {

        List<CustomerCountry> customerCountryList = new ArrayList<>();

        String prepStmtString = "SELECT country, count(customerId) as C FROM customer GROUP BY country ORDER BY C DESC";

        this.connectToDatabase();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                // create a new record for each record (table row) from the query result
                CustomerCountry record = new CustomerCountry(resultSet.getString("country"), resultSet.getInt("C"));

                // add the record to the list
                customerCountryList.add(record);
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        };

        this.closeDatabaseConnection();

        return customerCountryList;
    };

    @Override
    public List<CustomerSpender> getHighestSpendersCustomers() {
        List<CustomerSpender> customerList = new ArrayList<>();
        String prepStmtString = "SELECT customer.CustomerId, customer.FirstName, customer.LastName, invoice.Total FROM customer INNER JOIN invoice ON customer.CustomerId = invoice.CustomerId ORDER BY invoice.Total DESC";

        this.connectToDatabase(); // DatabaseConnector method

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customerList.add(
                        new CustomerSpender(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Total")
                ));
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // DatabaseConnector method

        return customerList;
    };

    @Override
    public CustomerGenre getCustomerPopularGenre(Customer customer) {
        CustomerGenre customerGenre = null;

        String prepStmtString = " SELECT customer.CustomerId, customer.FirstName, customer.LastName, genre.Name AS GenreName " +
                "FROM customer INNER JOIN invoice ON customer.CustomerId = invoice.CustomerId " +
                "INNER JOIN invoiceLine ON invoice.InvoiceId = invoiceLine.InvoiceId " +
                "INNER JOIN track ON invoiceLine.TrackId = track.TrackId " +
                "INNER JOIN genre ON genre.GenreId = track.GenreId " +
                "WHERE customer.CustomerId = ? GROUP BY genre.Name ORDER BY count(genre.Name) DESC LIMIT 1";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setString(1, customer.getCustomerId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customerGenre = new CustomerGenre(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("GenreName") // Genre name
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        return customerGenre;
    };
}
