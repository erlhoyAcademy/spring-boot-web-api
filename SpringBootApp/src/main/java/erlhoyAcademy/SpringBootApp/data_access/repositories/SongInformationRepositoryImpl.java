package erlhoyAcademy.SpringBootApp.data_access.repositories;

import erlhoyAcademy.SpringBootApp.data_access.DatabaseConnector;
import erlhoyAcademy.SpringBootApp.data_access.models.Artist;
import erlhoyAcademy.SpringBootApp.data_access.models.Genre;
import erlhoyAcademy.SpringBootApp.data_access.models.Song;
import erlhoyAcademy.SpringBootApp.data_access.models.SongArtistAlbumGenre;
import erlhoyAcademy.SpringBootApp.logging.ConsoleLogger;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SongInformationRepositoryImpl extends DatabaseConnector implements SongInformationRepository {


    @Override
    public List<Artist> getRandomArtists() {

        List<Artist> artists = new ArrayList<>();
        String prepStmtString = "SELECT * FROM artist LIMIT 5";

        this.connectToDatabase(); // initialize connection to db

        try {
            ResultSet resultSet = this.con.prepareStatement(prepStmtString).executeQuery();

            while(resultSet.next()) {
                artists.add(
                    new Artist(
                        resultSet.getString("ArtistId"), resultSet.getString("Name")
                    )
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // close db connection

        return artists;
    }

    @Override
    public List<Song> getRandomSongs() {
        List<Song> songList = new ArrayList<>();
        String prepStmtString = "SELECT TrackId, Name FROM track LIMIT 5";

        this.connectToDatabase(); // initialize connection to db

        try {
            ResultSet resultSet = this.con.prepareStatement(prepStmtString).executeQuery();

            while(resultSet.next()) {
                songList.add(
                        new Song(
                                resultSet.getString("TrackId"), resultSet.getString("Name")
                        )
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // close db connection

        return songList;
    }

    @Override
    public List<Genre> getRandomGenres() {
        List<Genre> genreList = new ArrayList<>();
        String prepStmtString = "SELECT * FROM genre LIMIT 5";

        this.connectToDatabase(); // initialize connection to db

        try {
            ResultSet resultSet = this.con.prepareStatement(prepStmtString).executeQuery();

            while(resultSet.next()) {
                genreList.add(
                        new Genre(
                                resultSet.getString("GenreId"), resultSet.getString("Name")
                        )
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection(); // close db connection

        return genreList;
    }

    @Override
    public SongArtistAlbumGenre getSongTableInfo(String searchTerm) {
        SongArtistAlbumGenre songTableInfo = new SongArtistAlbumGenre(searchTerm, "dummy", "dummy", "dummy", false);
        String prepStmtString = "SELECT track.Name, artist.Name AS ArtistName, album.Title, genre.Name AS GenreName FROM track INNER JOIN genre ON track.GenreId = genre.GenreId " +
                "INNER JOIN album ON track.AlbumId = album.AlbumId INNER JOIN artist ON album.ArtistId = artist.ArtistId WHERE track.Name" +
                " = ?";

        this.connectToDatabase();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(prepStmtString);
            preparedStatement.setString(1, searchTerm);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                songTableInfo = new SongArtistAlbumGenre(
                        resultSet.getString("Name"),
                        resultSet.getString("ArtistName"),
                        resultSet.getString("Title"),
                        resultSet.getString("GenreName"),
                        true
                );
            }
        } catch (SQLException e) {
            ConsoleLogger.log(e.getMessage());
        }

        this.closeDatabaseConnection();

        return songTableInfo;
    }
}
