package erlhoyAcademy.SpringBootApp.data_access.repositories;
import erlhoyAcademy.SpringBootApp.data_access.models.Artist;
import erlhoyAcademy.SpringBootApp.data_access.models.Genre;
import erlhoyAcademy.SpringBootApp.data_access.models.Song;
import erlhoyAcademy.SpringBootApp.data_access.models.SongArtistAlbumGenre;

import java.util.List;

public interface SongInformationRepository {

    public List<Artist> getRandomArtists();
    public List<Song> getRandomSongs();
    public List<Genre> getRandomGenres();
    public SongArtistAlbumGenre getSongTableInfo(String searchTerm);
}
