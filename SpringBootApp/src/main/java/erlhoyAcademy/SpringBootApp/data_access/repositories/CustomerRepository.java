package erlhoyAcademy.SpringBootApp.data_access.repositories;

import erlhoyAcademy.SpringBootApp.data_access.models.Customer;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerCountry;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerGenre;
import erlhoyAcademy.SpringBootApp.data_access.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository {

    public List<Customer> getCustomersFromDatabase();
    public Customer getCustomerFromDatabaseById(int id);
    public Customer getCustomerFromDatabaseByName(String name);
    public List<Customer> getCustomerPageFromDatabase(int limit, int offset);
    public boolean addCustomerToDatabase(Customer customer);
    public boolean updateCustomerFromDatabase(Customer customer);
    public List<CustomerCountry> getNumberOfCustomersInEachCountry();
    public List<CustomerSpender> getHighestSpendersCustomers();
    public CustomerGenre getCustomerPopularGenre(Customer customer);

}
