# Spring Boot Web API

A Spring Boot Web API application using Chinook_Sqlite sqlite database to access informations about songs, artist, albums, customers and more...
<br>
JDBC for connection to the database. The app is designed with SOLID best practises in mind.
<br>
The web app lets the user search for song names and see the results about them if the songs exist in the databse.

## Docker
A docker image of this application is available at Dockerhub.
<br><br>
docker pull erlhoyacademy/spring-boot-web-api

## Heroku
https://erlhoyacademy-springbootapp.herokuapp.com/
